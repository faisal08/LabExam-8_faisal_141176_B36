<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;
use App\BookTitle\BookTitle;
if(!isset($_SESSION))session_start();
echo Message::getMessage();
$objBookTitle=new BookTitle();
$objBookTitle->setData($_GET);
$oneData=$objBookTitle->view("obj");

//var_dump($oneData);  die;

?>

<div class="modal fade" id="MyModalnorm" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h3>Please enter Book Title and Author Name
                    <i class="fa fa-book fa-lg " aria-hidden="true"></i>
                </h3>

                <button type="button" class="close"
                        data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
            </div>

            <!-- Modal Body -->
            <div class="modal-body">

                <form action="update.php" id="bookform" method="post" role="form">
                    <input type ="hidden" name="id" value="<?php echo $oneData->id?>" >
                    <div class="form-group">
                        <label for="booktitle">Book Title</label>
                        <input type="text" class="form-control"name="booktitle"
                               id="booktitle" value="<?php echo $oneData->booktitle ?>"/>
                    </div>
                    <div class="form-group">
                        <label for="author_name">Author Name</label>
                        <input type="text" class="form-control"name="author_name"
                               id="author_name" value="<?php echo $oneData->author_name ?>"/>
                    </div>
                    <button type="submit" class="btn btn-info" ">update</button>


                </form>
            </div>
        </div>
    </div>
</div>

