<?php
require_once("../../../vendor/autoload.php");

use App\Email\Email;

$objemail  =  new Email();
$objemail->setData($_GET);
$oneData= $objemail->view("obj");
?>
<div class="modal fade" id="myModalnorm" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h3>Book Record
                    <i class="fa fa-book fa-lg " aria-hidden="true"></i>
                </h3>

                <button type="button" class="close"
                        data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
            </div>

            <!-- Modal Body -->
            <div class="modal-body">
                <table class="table table-responsive">

                    <thead>
                    <tr class="bg-primary">
                        <th>ID</th>
                        <th>User Name</th>
                        <th>EMAIL</th>

                    </tr>
                    </thead>
                    <?php
                    echo "<tbody>";
                    echo "<tr>";
                    echo "<td>".$oneData->id."</td>";
                    echo "<td>".$oneData->username."</td>";
                    echo "<td>".$oneData->email."</td>";
                    echo "</tr>";
                    echo "</tbody>";

                    ?>
                </table>
            </div>
            <div class="modal-footer">
                <a class="btn" data-dismiss="modal">Close</a>
            </div>
        </div>
    </div>
</div>
