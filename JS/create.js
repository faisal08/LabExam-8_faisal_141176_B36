$(document).ready(function() {

    // Generate a simple captcha
    $('#bookform').bootstrapValidator({

        message: 'This value is not valid',

        fields: {
            booktitle: {
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: 'The username is required and can\'t be empty'
                    }
                }
            },
            author_name: {
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: 'The username is required and can\'t be empty'
                    }
                }
            }
        }
    });
});

