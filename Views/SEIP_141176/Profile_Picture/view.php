<?php
require_once("../../../vendor/autoload.php");

use App\Profile_Picture\Profile_Picture;

$objIMAGE  =  new Profile_Picture();
$objIMAGE->setData($_GET);
$oneData= $objIMAGE->view("obj");


?>
<div class="modal fade" id="myModalnorm" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h3>Book Record
                    <i class="fa fa-book fa-lg " aria-hidden="true"></i>
                </h3>

                <button type="button" class="close"
                        data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
            </div>

            <!-- Modal Body -->
            <div class="modal-body">
                <table class="table table-responsive">

                    <thead>
                    <tr class="bg-primary">
                        <th>ID</th>
                        <th>User Name</th>
                        <th>Image</th>

                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                    <td><?php echo $oneData->id;?></td>
                    <td><?php echo $oneData->username;?></td>
                    <td><img src="<?php echo "upload/".$oneData->image_url;?>"alt="image" height="200px" width="200px"></td>
                    </tr>
                    </tbody>


                </table>
            </div>
            <div class="modal-footer">
                <a class="btn" data-dismiss="modal">Close</a>
            </div>
        </div>
    </div>
</div>
