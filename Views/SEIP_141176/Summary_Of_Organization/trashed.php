<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;
use App\Summary_Of_Organization\Summary_Of_Organization;
$objSummary = new Summary_Of_Organization();

$allData = $objSummary->trashed("obj");
$serial = 1;
?>

<!DOCTYPE html>
<html lang="en">

<head><title>Atomic Project </title>
    <link href="../../../style/style.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="../../../bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../bootstrap/js/bootstrap.min.js">
    <link rel="stylesheet" href="../../../bootstrap/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="../../../bootstrap/css/bootstrap-datepicker3.min.css">
    <script src="../../../bootstrap/js/jquery.min.js"></script>
    <script src="../../../bootstrap/js/bootstrap.min.js"></script>
    <script src="../../../bootstrap/js/bootstrap-datepicker.min.js"></script>
    <script src="../../../JS/birthday.js"></script>
    <script type="text/javascript" src="../../../bootstrap/js/bootstrapValidator.js"></script>
    <link rel="stylesheet" href="../../../bootstrap/css/bootstrapValidator.css"/>
    <link rel="stylesheet" href="../../../font-awesome-4.7.0/css/font-awesome.min.css"/>
</head>
<body>
<div class="container" id="contain">

    <div class="jumbotron" id="OrgSummary">
        <h2></h2>


    </div>

    <form class="navbar-form navbar-right">
        <div class="form-group">
            <input type="text" class="form-control" placeholder="Search">
        </div>
        <button type="submit" class="btn btn-default">Search</button>
    </form>
    <div class="page-header" id="Mmessage">
        <?php echo Message::message();?>
    </div>
    <div class="row">
        <div class="col-md-3 col-sm-3 col-lg-3" >
            <div class="well" id="navbar">
                <ul class="nav nav-pills nav-stacked" role="tablist" >
                    <li ><a href="../BookTitle/create.php">Book Title</a></li>
                    <li ><a href="../Birthdate/create.php">Birthday</a></li>
                    <li><a href="../City/create.php">City</a></li>
                    <li><a href="../Email/create.php">Email</a></li>
                    <li><a href="../Gender/create.php">Gender</a></li>
                    <li><a href="../Hobbies/create.php">Hobbies</a></li>
                    <li><a href="../Profile_Picture/create.php">Profile Picture</a></li>
                    <li class="active"><a href="create.php">Summary of Organization</a></li>
                </ul>
            </div>
        </div>

        <div class="col-md-9 col-lg-9 col-sm-9" >
            <div class="panel panel-default">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                       <a href="create.php"> <button type="button" class="btn btn-info"  
                            <span class="glyphicon glyphicon-plus">Back</span></button></a>

                    <div class="nav navbar-right">
                        <label >booklist per page</label>
                        <select class="bg-primary">
                            <option id="select">5</option>
                            <option id="select">10</option>
                            <option id="select">15</option>

                        </select>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="well" id="navbar">
                    <table class="table table-responsive">

                        <thead>
                        <tr class="bg-primary">
                            <th style="width:7%">SL.no</th>
                            <th style="width:7%">ID</th>
                            <th style="width:20%">Organization Name</th>
                            <th style="width:30%">Organization Summary</th>
                            <th style="width:36%">Action</th>
                        </tr>
                        </thead>
                        <?php   foreach($allData as $onedata){
                        $limit=100;
                        echo "<tbody>";
                        echo "<tr>";?>
                            <td ><?php echo $serial;?></td>
                            <td><?php echo  $onedata->id;?></td>
                            <td><?php echo  $onedata->Org_Name;?></td>
                            <td ><?php $des= $onedata->Org_Summary;
                                echo substr( $des,0,$limit);
                                echo "<a href=view.php?>read more</a>";?>
                            </td>
                            <?php

                            echo "<td>";

                            echo "<a href='recover.php?id= $onedata->id'><button class='btn btn-success'>Recover</button></a> ";

                            echo "<a href='delete.php?id= $onedata->id'><button class='btn btn-danger'>Delete</button></a> ";


                            echo "</td>";

                            echo "</tr>";
                            echo "</tbody>";
                            $serial++;


                        // echo  "</ul>";


                        }
                        ?>

                    </table>
                </div>
                <nav aria-label="..." class="nav navbar-right">
                    <ul class="pager" >
                        <li><a href="#">Previous</a></li>
                        <li><a href="#">Next</a></li>
                    </ul>
                </nav>

            </div>
        </div>
    </div>

    </div>







</body>

</html>