<?php
require_once("../../../vendor/autoload.php");

use App\Gender\Gender;

$objGender  =  new Gender();
$objGender->setData($_GET);
$oneData= $objGender->view("obj");
?>
<div class="modal fade" id="myModalnorm" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h3>Gender Record
                    <i class="fa fa-male fa-lg " aria-hidden="true"></i>
                </h3>

                <button type="button" class="close"
                        data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
            </div>

            <!-- Modal Body -->
            <div class="modal-body">
                <table class="table table-responsive">

                    <thead>
                    <tr class="bg-primary">
                        <th>ID</th>
                        <th>User Name</th>
                        <th>Gender</th>

                    </tr>
                    </thead>
                    <?php
                    echo "<tbody>";
                    echo "<tr>";
                    echo "<td>".$oneData->id."</td>";
                    echo "<td>".$oneData->username."</td>";
                    echo "<td>".$oneData->gender."</td>";
                    echo "</tr>";
                    echo "</tbody>";

                    ?>
                </table>
            </div>
            <div class="modal-footer">
                <a class="btn" data-dismiss="modal">Close</a>
            </div>
        </div>
    </div>
</div>
