<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;
use App\Email\Email;
$serial=1;
$objCity=new Email();
$alldata=$objCity->index("obj");
?>

<!DOCTYPE html>
<html lang="en">

<head><title>Atomic Project </title>
    <link href="../../../style/style.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="../../../bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../bootstrap/js/bootstrap.min.js">
    <script src="../../../bootstrap/js/jquery.min.js"></script>
    <script src="../../../bootstrap/js/bootstrap.min.js"></script>
    <script src="../../../JS/email.js"></script>
    <script type="text/javascript" src="../../../bootstrap/js/bootstrapValidator.js"></script>
    <link rel="stylesheet" href="../../../bootstrap/css/bootstrapValidator.css"/>
    <link rel="stylesheet" href="../../../font-awesome-4.7.0/css/font-awesome.min.css"/>
    <script type="text/javascript">
        function load(thediv,thefile) {
            if (window.XMLHttpRequest){
                xmlhttp=new XMLHttpRequest();
            }
            else {
                xmlhttp=new ActiveXObject('Microsoft.XMLHTTP');
            }
            xmlhttp.onreadystatechange=function () {
                if (xmlhttp.readyState==4 && xmlhttp.status==200){
                    document.getElementById(thediv).innerHTML=xmlhttp.responseText;
                }
            }
            xmlhttp.open('GET',thefile,true);
            xmlhttp.send();
            xmlhttp.close();
        }

    </script>
</head>
<body>
<div class="container" id="contain">

    <div class="jumbotron" id="EMAIL">
        <h2></h2>


    </div>

    <form class="navbar-form navbar-right">
        <div class="form-group">
            <input type="text" class="form-control" placeholder="Search">
        </div>
        <button type="submit" class="btn btn-default">Search</button>
    </form>
    <div class="page-header" id="Mmessage">
<?php echo Message::message(); ?>
    </div>
    <div class="row">
        <div class="col-md-3 col-sm-3 col-lg-3" >
            <div class="well" id="navbar">
                <ul class="nav nav-pills nav-stacked" role="tablist" >
                    <li ><a href="../BookTitle/create.php">Book Title</a></li>
                    <li><a href="../Birthdate/create.php">Birthday</a></li>
                    <li><a href="../City/create.php">City</a></li>
                    <li class="active"><a href="create.php">Email</a></li>
                    <li><a href="../Gender/create.php">Gender</a></li>
                    <li><a href="../Hobbies/create.php">Hobbies</a></li>
                    <li><a href="../Profile_Picture/create.php">Profile Picture</a></li>
                    <li><a href="../Summary_Of_Organization/create.php">Summary of Organization</a></li>
                </ul>
            </div>
        </div>

        <div class="col-md-9 col-lg-9 col-sm-9" >
            <div class="panel panel-default">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <button type="button" class="btn btn-info"  data-toggle="modal" data-target="#myModalNorm">
                            <span class="glyphicon glyphicon-plus">create new</span></button>
                        <a href="trashed.php"> <button type="button" class="btn btn-info"  >
                                <span class="glyphicon glyphicon-trash">Trashed List</span></button></a>

                        <div class="nav navbar-right">
                            <label >booklist per page</label>
                            <select class="bg-primary">
                                <option id="select">5</option>
                                <option id="select">10</option>
                                <option id="select">15</option>

                            </select>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="well" id="navbar">
                        <table class="table table-responsive">

                            <thead>
                            <tr class="bg-primary">
                                <th style="width:7%">SL.no</th>
                                <th style="width:7%">ID</th>
                                <th style="width:20%">User Name</th>
                                <th style="width:30%">Email</th>
                                <th style="width:36%">Action</th>
                            </tr>
                            </thead>
                            <?php   foreach($alldata as $onedata){
                                echo "<tbody>";
                                echo "<tr>";
                                echo "<td>".$serial."</td>";
                                echo "<td>".$onedata->id."</td>";
                                echo "<td>".$onedata->username."</td>";
                                echo "<td>".$onedata->email."</td>";
                                echo "<td>";
                                //echo "<ul class='breadcrumb '>";
                                echo "<a ><button class='btn btn-info' data-toggle='modal' data-target='#myModalnorm' 
                                               onclick=\"load('Mmessage','view.php?id=$onedata->id');\">view</button> </a>";
                              //  echo "<a ><button class='btn btn-primary' data-toggle='modal' data-target='#MyModalnorm'
                                       //         onclick=\"load('Mmessage','editt.php?id=$onedata->id');\">edit</button> </a>";
                                echo "<a href='edit.php?id=$onedata->id'><button class='btn btn-primary'>edit</button> </a>";
                                echo "<a href='trash.php?id=$onedata->id'><button class='btn btn-success'>Trash</button></a> ";
                                echo "<a href='delete.php?id=$onedata->id'><button class='btn btn-danger'>delete</button> </a>";
                                echo "</td>";

                                echo "</tr>";

                                $serial++;



                                // echo  "</ul>";
                                echo "</tr>";
                                echo "</tbody>";
                            }
                            ?>

                            </tbody>
                        </table>
                    </div>
                    <nav aria-label="..." class="nav navbar-right">
                        <ul class="pager" >
                            <li><a href="#">Previous</a></li>
                            <li><a href="#">Next</a></li>
                        </ul>
                    </nav>

                </div>
            </div>
        </div>
        <div class="modal fade" id="myModalNorm" tabindex="-1" role="dialog"
             aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h3>Please enter Book Title and Author Name
                            <i class="fa fa-book fa-lg " aria-hidden="true"></i>
                        </h3>

                        <button type="button" class="close"
                                data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                            <span class="sr-only">Close</span>
                        </button>
                    </div>

                    <!-- Modal Body -->
                    <div class="modal-body">

                        <form action="store.php" id="emailform" method="post" role="form">
                            <div class="form-group">
                                <label for="username">User Name</label>
                                <input type="text" class="form-control"name="username"
                                       id="usermail" placeholder="Enter username"/>
                            </div>
                            <div class="form-group">
                                <label for="user_email">Email</label>
                                <input type="text" class="form-control"name="user_email"
                                       id="email" placeholder="Enter email"/>
                            </div>
                            <button type="submit" class="btn btn-info" >Submit</button>


                        </form>
                    </div>
                </div>
            </div>
        </div>



    </div>



</body>

</html>