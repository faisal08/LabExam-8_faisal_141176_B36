$(document).ready(function() {

    // Generate a simple captcha
    $('#imageform').bootstrapValidator({

        message: 'This value is not valid',

        fields: {
            username: {
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: 'The username is required and can\'t be empty'
                    }
                }
            },
            image: {
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: 'The image is required and can\'t be empty'
                    }
                }
            }
        }
    });
});