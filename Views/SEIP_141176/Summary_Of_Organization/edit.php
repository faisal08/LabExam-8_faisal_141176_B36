<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;
use App\Summary_Of_Organization\Summary_Of_Organization;
if(!isset($_SESSION))session_start();
$objSUMMARY=new Summary_Of_Organization();
$objSUMMARY->setData($_GET);
$oneData=$objSUMMARY->view("obj");

?>

<!DOCTYPE html>
<html lang="en">

<head><title>Atomic Project </title>
    <link href="../../../style/style.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="../../../bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../bootstrap/js/bootstrap.min.js">
    <script src="../../../bootstrap/js/jquery.min.js"></script>
    <script src="../../../bootstrap/js/bootstrap.min.js"></script>
    <script src="../../../JS/create.js"></script>
    <script type="text/javascript" src="../../../bootstrap/js/bootstrapValidator.js"></script>
    <link rel="stylesheet" href="../../../bootstrap/css/bootstrapValidator.css"/>
    <link rel="stylesheet" href="../../../font-awesome-4.7.0/css/font-awesome.min.css"/>

</head>
<body>
<div class="container" id="contain">

    <div class="jumbotron" id="header">
        <h2>welcome to book title</h2>


    </div>

    <form class="navbar-form navbar-right">
        <div class="form-group">
            <input type="text" class="form-control" placeholder="Search">
        </div>
        <button type="submit" class="btn btn-default">Search</button>
    </form>
    <div class="row">
        <div class="page-header" id="Mmessage">
            <?php echo Message::message(); ?>
        </div></div>
    <div class="row">
        <div class="col-md-3 col-sm-3 col-lg-3" >
            <div class="well" id="navbar">
                <ul class="nav nav-pills nav-stacked" role="tablist" >
                    <li class="active"><a href="create.php">Book Title</a></li>
                    <li><a href="../Birthdate/create.php">Birthday</a></li>
                    <li><a href="../City/create.php">City</a></li>
                    <li><a href="../Email/create.php">Email</a></li>
                    <li><a href="../Gender/create.php">Gender</a></li>
                    <li><a href="../Hobbies/create.php">Hobbies</a></li>
                    <li><a href="../Profile_Picture/create.php">Profile Picture</a></li>
                    <li><a href="../Summary_Of_Organization/create.php">Summary of Organization</a></li>
                </ul>
            </div>
        </div>

        <div class="col-md-9 col-lg-9 col-sm-9" >
            <div class="panel panel-default">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <button type="button" class="btn btn-info"  data-toggle="modal" data-target="#myModalNorm">
                            <span class="glyphicon glyphicon-plus">create new</span></button>

                        <div class="nav navbar-right">
                            <label >booklist per page</label>
                            <select class="bg-primary">
                                <option id="select">5</option>
                                <option id="select">10</option>
                                <option id="select">15</option>

                            </select>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="well " id="navbar">
                        <div class="form-top">
                            <div class="form-top-left">
                                <h3>Edit_Organization Summary<i class="fa fa-book"></i></h3>
                            </div>
                        </div>
                        <div class="form-bottom">
                            <form action="update.php" id="summaryform" method="post" role="form">
                                <input type ="hidden" name="id" value="<?php echo $oneData->id?>" >
                                <div class="form-group">
                                    <label for="org_name">Organization Name</label>
                                    <input type="text" class="form-control"name="org_name"
                                           id="OrgName" value="<?php echo $oneData->Org_Name; ?>"/>
                                </div>
                                <div class="form-group">
                                    <label for="org_summary">Organization Summary</label>
                                    <textarea cols="10" rows="5" class="form-control"name="org_name"
                                              id="OrgName" placeholder="<?php echo $oneData->Org_Summary; ?>"></textarea>
                                </div>
                                <button type="submit" class="btn btn-info">Submit</button>


                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>



</body>

</html>